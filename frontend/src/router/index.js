import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/logowanie',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/rejestracja',
    name: 'Register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/konto',
    name: 'Account',
    component: () => import('../views/Account.vue')
  },
  {
    path: '/subreddit/:subredditData',
    name: 'Subreddit',
    component: () => import('../views/Subreddit.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
