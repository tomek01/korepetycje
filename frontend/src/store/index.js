import { createStore } from 'vuex'
import axios from 'axios'
import data from './data'

export default createStore({
  state: {
    userData: null,
    isUserLogged: false,
    posts: data.posts,
    postsVotes: data.postsVotes,
    comments: data.comments,
    redditUsers: data.redditUsers,
    subreddits: data.subreddits,
    subredditsModerators: data.subredditsModerators,
    subredditsUsers: data.subredditsUsers,
    listView: 'card',
    listViewOptions: [
      { text: "karty", value: "card" },
      { text: "listy", value: "list" },
    ]
  },
  mutations: {
    userData: (state, data) => state.userData = data,
    isUserLogged: (state, data) => state.isUserLogged = data,
    posts: (state, data) => state.posts = data,
    postsVotes: (state, data) => state.postsVotes = data,
    comments: (state, data) => state.comments = data,
    redditUsers: (state, data) => state.redditUsers = data,
    subreddits: (state, data) => state.subreddits = data,
    subredditsModerators: (state, data) => state.subredditsModerators = data,
    subredditsUsers: (state, data) => state.subredditsUsers = data,
    listView: (state, data) => state.listView = data
  },
  actions: {
    getInitialData({ commit }) {
      axios
        .get("http://localhost:8081/api/initial-data")
        .then(res => {
          commit('posts', res.data.posts);
          commit('posts', res.data.posts);
          commit('postsVotes', res.data.postsVotes);
          commit('comments', res.data.comments);
          commit('subreddits', res.data.subreddits);
          commit('subredditsModerators', res.data.subredditsModerators);
          commit('subredditsUsers', res.data.subredditsUsers);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
    getPosts({ commit }) {
      axios
        .get("http://localhost:8081/api/posts")
        .then(res => {
          commit('posts', res.data);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
    getPostsVotes({ commit }) {
      axios
        .get("http://localhost:8081/api/posts-votes")
        .then(res => {
          commit('postsVotes', res.data);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
    getComments({ commit }) {
      axios
        .get("http://localhost:8081/api/comments")
        .then(res => {
          commit('comments', res.data);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
    getRedditUsers({ commit }) {
      axios
        .get("http://localhost:8081/api/reddit-users")
        .then(res => {
          commit('redditUsers', res.data);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
    getSubreddits({ commit }) {
      axios
        .get("http://localhost:8081/api/subreddits")
        .then(res => {
          commit('subreddits', res.data);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
    getSubredditsModerators({ commit }) {
      axios
        .get("http://localhost:8081/api/subreddits-moderators")
        .then(res => {
          commit('subredditsModerators', res.data);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
    getSubredditsUsers({ commit }) {
      axios
        .get("http://localhost:8081/api/subreddits-users")
        .then(res => {
          console.log(res.data)
          commit('subredditsUsers', res.data);
        })
        .catch(err => {
          console.log('Problem z pobieraniem')
        });
    },
  }
})
