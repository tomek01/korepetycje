function routes(app) {

    app.get('/api/subreddits', async (req, res) => {
        try {
            const query = await app.db.query(`SELECT * FROM subreddit`);
            if (query && query.rows.length > 0) {
                return res.send(query.rows);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

    app.get('/api/subreddits-moderators', async (req, res) => {
        try {
            const query = await app.db.query(`SELECT * FROM subreddit_moderator`);
            if (query && query.rows.length > 0) {
                return res.send(query.rows);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

    app.get('/api/subreddits-users', async (req, res) => {
        try {
            const query = await app.db.query(`SELECT * FROM subreddit_user`);
            if (query && query.rows.length > 0) {
                return res.send(query.rows);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

}

module.exports = routes;