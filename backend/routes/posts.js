function routes(app) {

    app.get('/api/posts', async (req, res) => {
        try {
            const query = await app.db.query(`SELECT * FROM post`);
            if (query && query.rows.length > 0) {
                return res.send(query.rows);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

    app.get('/api/posts-votes', async (req, res) => {
        try {
            const query = await app.db.query(`SELECT * FROM post_vote`);
            if (query && query.rows.length > 0) {
                return res.send(query.rows);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

    app.get('/api/comments', async (req, res) => {
        try {
            const query = await app.db.query(`SELECT * FROM comment`);
            if (query && query.rows.length > 0) {
                return res.send(query.rows);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

}

module.exports = routes;