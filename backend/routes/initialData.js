function routes(app) {

    app.get('/api/initial-data', async (req, res) => {
        try {
            const postsVotesQuery = await app.db.query(`SELECT * FROM post_vote`);
            const commentsQuery = await app.db.query(`SELECT * FROM comment`);
            const postsQuery = await app.db.query(`SELECT * FROM post`);
            const subredditsQuery = await app.db.query(`SELECT * FROM subreddit`);
            const subredditsModeratorsQuery = await app.db.query(`SELECT * FROM subreddit_moderator`);
            const subredditsUsersQuery = await app.db.query(`SELECT * FROM subreddit_user`);
            const redditUsersQuery = await app.db.query(`SELECT * FROM reddit_user`);
            if (
                postsVotesQuery && postsVotesQuery.rows.length > 0
                && commentsQuery && commentsQuery.rows.length > 0
                && postsQuery && postsQuery.rows.length > 0
                && subredditsQuery && subredditsQuery.rows.length > 0
                && subredditsModeratorsQuery && subredditsModeratorsQuery.rows.length > 0
                && subredditsUsersQuery && subredditsUsersQuery.rows.length > 0
                && redditUsersQuery && redditUsersQuery.rows.length > 0
            ) {
                let initialData = {
                    postsVotes: postsVotesQuery.rows,
                    comments: commentsQuery.rows,
                    posts: postsQuery.rows,
                    subreddits: subredditsQuery.rows,
                    subredditsModerators: subredditsModeratorsQuery.rows,
                    subredditsUsers: subredditsUsersQuery.rows,
                    redditUsers: redditUsersQuery.rows,
                };
                return res.send(initialData);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

}

module.exports = routes;