function routes(app) {

    app.get('/api/reddit-users', async (req, res) => {
        try {
            const query = await app.db.query(`SELECT * FROM reddit_user`);
            if (query && query.rows.length > 0) {
                return res.send(query.rows);
            } else {
                return res.status(401);
            }
        } catch (error) {
            return res.status(401);
        }
    });

}

module.exports = routes;