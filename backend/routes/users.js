const passport = require("passport");

function routes(app) {

    app.post('/api/register', async (req, res) => {
        try {
            const { email, password } = req.body;
            if (email, password) {
                const emailRegex = /^(?:[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2,}(?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
                const passwordRegex = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z]){8,}/;
                if (emailRegex.test(email) && passwordRegex.test(password)) {
                    const checkIfEmailExist = await app.db.query(`SELECT email FROM reddit_user WHERE email = $1`, [email]);
                    if (checkIfEmailExist && checkIfEmailExist.rows.length === 1) {
                        return res.send('Użytkownik z takim e-mailem już istnieje.');
                    } else {
                        const nickname = email.match(/^(.+)@/)[1];
                        const query = await app.db.query(`INSERT INTO reddit_user (email, password, nickname) VALUES ($1, $2, $3) RETURNING email`, [email, password, nickname]);
                        if (query.rows.length === 1) {
                            return res.send('Użytkownik został stworzony. Proszę się zalogować.');
                        } else {
                            return res.status(401);
                        }
                    }
                } else {
                    return res.send('Niepoprawny e-mail lub hasło.');
                }
            } else {
                return res.send('Brakujący e-mail lub hasło.');
            }
        } catch (error) {
            return res.status(401);
        }
    });

    app.post('/api/login', async (req, res) => {
        try {
            const { email, password } = req.body;
            if (email, password) {
                const emailRegex = /^(?:[A-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2,}(?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
                const passwordRegex = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z]){8,}/;
                if (emailRegex.test(email) && passwordRegex.test(password)) {
                    const query = await app.db.query(`SELECT id, email, password, nickname FROM reddit_user WHERE email = $1 AND password = $2`, [email, password]);
                    if (query && query.rows.length === 1) {
                        return res.send({ loggedIn: true, data: query.rows[0] });
                    } else {
                        return res.send('Niepoprawny e-mail lub hasło.');
                    }
                } else {
                    return res.send('Niepoprawny e-mail lub hasło.');
                }
            } else {
                return res.send('Brakujący e-mail lub hasło.');
            }
        } catch (error) {
            return res.status(401);
        }
    });

    app.put('/api/change-password', async (req, res) => {
        try {
            const { email, password, newPassword } = req.body;
            if (email, password, newPassword) {
                const passwordRegex = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z]){8,}/;
                if (passwordRegex.test(password)) {
                    const query = await app.db.query(`UPDATE reddit_user SET password = $1 WHERE password = $2 AND email = $3 RETURNING email;`, [newPassword, password, email]);
                    if (query && query.rows.length === 1) {
                        return res.send({
                            passwordChanged: true,
                            message: 'Hasło zostało zmienione.'
                        });
                    } else {
                        return res.send('Brakuje danych. Odśwież stronę i spróbuj jeszcze raz.');
                    }
                } else {
                    return res.send('Niepoprawne hasło.');
                }
            } else {
                return res.send('Brakuje danych. Odśwież stronę i spróbuj jeszcze raz.');
            }
        } catch (error) {
            return res.status(401);
        }
    });

}

module.exports = routes;