const express = require('express');
const app = express();
const cors = require('cors');
const session = require('express-session');
const passport = require('passport');
const db = require('./config/db');

app.db = db;
require('./config/passport')(passport, app);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(session({ secret: 'secret', resave: false, saveUninitialized: false }));
app.use(cors({ origin: 'http://localhost:8080', credentials: true }));
app.use(passport.initialize());
app.use(passport.session());

require('./routes')(app);

app.listen(8081);