const LocalStrategy = require('passport-local').Strategy;

module.exports = function (passport, app) {
    passport.use(
        new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
            app.db.query(`SELECT * FROM reddit_user WHERE email = $1`, [email])
                .then(query => {
                    if (query.rows && query.rows.length > 0) {
                        const user = query.rows[0];
                        if (password == user.password) {
                            return done(null, user);
                        } else {
                            return done(null, false, 'Niepoprawny e-mail lub hasło.')
                        }
                    } else {
                        return done(null, false, 'Konto z takim e-mailem nie istnieje.');
                    }
                })
                .catch(err => {
                    done(null, false, 'Wystąpił błąd na serwerze.');
                });
        })
    );

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        app.db.query(`SELECT * FROM reddit_user WHERE id = $1`, [id])
            .then(query => {
                if (query.rows && query.rows.length > 0) {
                    const user = query.rows[1];
                    return done(null, user);
                } else {
                    return done(null, false, 'Takie konto nie istnieje.');
                }
            })
            .catch(err => {
                done(null, false, 'Wystąpił błąd na serwerze.');
            });
    });
}