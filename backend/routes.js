function routes(app) {
    require('./routes/users')(app);
    require('./routes/reddit')(app);
    require('./routes/subreddits')(app);
    require('./routes/posts')(app);
    require('./routes/initialData')(app);
}

module.exports = routes;